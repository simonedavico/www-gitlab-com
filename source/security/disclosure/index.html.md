---
layout: markdown_page
title: Responsible Disclosure Policy
---

Please email `security@gitlab.com` to report any security vulnerabilities. We will
acknowledge receipt of your vulnerability report the next business day and strive to send you regular updates about our progress. If you're curious about the status of your disclosure please feel free to email us again. If you want to encrypt your disclosure email download our key from [MIT PGP key server](https://pgp.mit.edu/pks/lookup?op=get&search=0xA11CC6B586EF1357), find it [below](#public-gpg-key), or email us to have us send it to you.

Alternatively you may also send us your report via [HackerOne](https://hackerone.com/gitlab).

Please refrain from requesting compensation for reporting vulnerabilities. If you want we will [publicly acknowledge](/security/vulnerability-acknowledgements/) your responsible disclosure. We also try to make the confidential issue public after the vulnerability is announced, for an example see our [impersonation feature issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/15548). HackerOne also makes the bug reports [public after 30 days](https://hackerone.com/disclosure-guidelines) if neither party objects, for an example see [the report for a persistent XSS on public project page](https://hackerone.com/reports/129736).

You are not allowed to search for vulnerabilities on GitLab.com itself. GitLab
is open source software, you can install a copy yourself and test against that. You can either download [CE](/install/), [EE](/downloads-ee/), or the [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit).
If you want to perform testing without setting GitLab up yourself please contact us to
arrange access to a staging server.

You can find more details on how we handle [security releases here](https://gitlab.com/gitlab-org/release-tools/blob/master/doc/security.md).

On our website you can find more about [the availability and security of GitLab.com](/gitlab-com/#faq). Security issues that are not vulnerabilities can be seen on [our public issue tracker](https://gitlab.com/gitlab-org/gitlab-ce/issues?label_name%5B%5D=security).

## Confidential issues

When a vulnerability is suspected or discovered we create a [confidential ~security issue](/handbook/engineering/security/#creating-new-security-issues) to track it internally.
Security patches are pushed to [dev.gitlab.org](https://dev.gitlab.org), which is not publicly accessible, and merged into the `security` branch.
They should not appear on [GitLab.com](https://gitlab.com) until the security release has been announced and updated packages are available.

Details can be found in our [critical release process](/handbook/engineering/critical-release-process/).

## Red Team Rules of Engagement

If you want to conduct red teaming against GitLab you will need written permission upfront.
You can apply by emailing security@gitlab.com your plans and experience.
You need to get a written authorization letter from our Directory of Security.
While you are engaged in red teaming activities you should coordinate with the Security Team so escalation (law enforcement, etc.) can be avoided.
The Security Team will notify the Infrastructure Team as well as the VP of Engineering so that awareness is maintained.

### Public GPG Key

 * `GitLab Security <security@gitlab.com>`
 * ID: A11CC6B586EF1357
 * Fingerprint BD6DA8467F8BA81203672C83A11CC6B586EF1357

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQENBFuql0YBCADpCTagFlvldEfM/yZaCZ8C/CxrqnOfdAK6FDqQrZpH/cvvoauL
W09qXXsi1yZOWOjbKX/ax7qw/7Z2aAzvWOW+epBfmA7lyJOwtQfF81wKkqPSF+tK
dlibgFX6QAgrR6G9IVOy72/MD/T2TnTL40zuYC3p23h7T5wkLqXUaHa9Fc3M2OOM
N4TcIxfz5ypgEgBbF/VwCgnmKyUWxy7AXmLwFywCRJY60zKf57OAxJPn+0XAwh34
JJKC+CTh15RAh/rh6Oh2ihvjltybJgThQp2F72jNAgzkAflwqQeF7psjKXQxvNR7
fQVGndHG7/H1HUoOSRsRHvgLtJCDVsILS6STABEBAAG0JUdpdExhYiBTZWN1cml0
eSA8c2VjdXJpdHlAZ2l0bGFiLmNvbT6JAVQEEwEIAD4WIQS9bahGf4uoEgNnLIOh
HMa1hu8TVwUCW6qXRgIbAwUJA8JnAAULCQgHAgYVCgkICwIEFgIDAQIeAQIXgAAK
CRChHMa1hu8TV2fKB/4ikVC42Xc/l5eh5TZMajYtr5C69bYdHOWkEtSCMXgp3x2X
Ezy59cTxWSsD76hTr2ubSDzDQZ21eFlq6mZ98hsz9+y64C/3bO/3LFF6itPamznS
oAYT9zFpNZVmeH4+0SLZBaDlsQOZ1EqjRGVySWRSCC/hnMfugCukEdQJrmq/QXvn
2D4/M3XEMHh/c2cExYYxafIuQ6zaxbISzfQOLFEZWNPk1foQVJd8XANYsglW8sdI
BczMAVNVzLZ6z1feD+cXf1N3SVUNpocMcQW7sWkqZhw0TsowrMKSuPMHcEzZLPlH
qs1meTUCWbMFLKwN1Byl5JOOTdKBwmp72oWOmDkYuQENBFuql0YBCAC7v0pbMG9u
jjoPrQMngmKD26sMMCj4Tz+pOWhpPgvthv+0ufgsAwFA+Oc19Xdt+MaOhwLfQp8R
meVpsxscZfG+2kzVpytl7edHpNxwy9vS1z7iTuEwBjAk0Fr9D1u7uUvYAEvkXXw6
2/WPZo95aSuTvKMussHUH1hxjDahHRyYn1j8Q6w7mVI9MtV8eCo2qpJjnJrIe3UK
LDyXU9fbZws1Fzv0f71VroXPOTOs7FjEiD7a8d6Y7d8zI8gInCfw+b5Te1Qt5BqW
eyRZpGtvWI1/gvOu8lCGa0FxXTHl+n3nQTbPruRC+lWWqC9uQwykCmewMilZA+A+
2/7xUCSPnVW9ABEBAAGJATwEGAEIACYWIQS9bahGf4uoEgNnLIOhHMa1hu8TVwUC
W6qXRgIbDAUJA8JnAAAKCRChHMa1hu8TV1QeB/4gWi/KjcXgrxt9paHuGHB46rRs
fowa9Q2EkeFPCSoUQGmOYN1En/Si8rZ/VZyszFvHAQD2a56UizSorOTAGNVG5jUK
1kaCalQiKhCKWf8oUS7Cu+GXS9ESZHa4dfHIx5w+qED9tM6Nd6pNK4v4JFPQwN/F
W4jkfRbwvreLreACHZ1OY2ZNhW8T2g8d3Vqv+D79BszIBzzpR0YGXmKqbI5zrxkU
jdfBHttzAYPDrBxvqAcXZF6vKXRupxyrmYBwkygMM2JX3YoU7echKqQuKnsJPWKp
6v8dhgsUbu6r2UoYWmk1ha7USM76xXbYc6buHDEjogoZF1ydDyQhB3Um4aFP
=LHOO
-----END PGP PUBLIC KEY BLOCK-----
```
