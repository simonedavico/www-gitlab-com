---
layout: markdown_page
title: "PR.1.01 - Background Checks Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# PR.1.01 - Background Checks

## Control Statement

New hires are required to pass a background check as a condition of their employment. When not performed, documentation outlining the exception is retained.

## Context

GitLab is concerned about the safety of all GitLab team-members and about maintaining appropriate controls to ensure that assets of GitLab and our customer relationships and information are protected. To reduce these risks, GitLab will obtain and review background information of covered prospective, and, as applicable, current GitLab team-members.

This information comes from our [handbook](/handbook/people-group/code-of-conduct/#background-checks) and more details about our background check process can be found there.

## Scope

Background checks apply to all GitLab team-members that do not live in a country or state that conflicts with this practice.

## Ownership

Control owner:
* `People Operations`

Process owner:
* People Operations

The GitLab Candidate Experience team manages the background process and retains all records relating to this control.

## Guidance

The scope of the background check performed is at the discretion of the Company. This control merely states that we apply this control to all GitLabbers without a documented reason why a background check can't be performed per local law.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Background Checks control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/860).

### Policy Reference

## Framework Mapping

* ISO
  * A.7.1.1
* SOC2 CC
  * CC1.1
  * CC1.4
  * CC1.5
* PCI
  * 12.7
