---
layout: markdown_page
title: "Benefits"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc}

- TOC
{:toc}

----

NOTE: Our contractor agreements and employment contracts are all on the [Contracts](/handbook/contracts/) page.

## Entity Specific Benefits
- [GitLab BV (Netherlands)](/handbook/benefits/bv-benefits-netherlands)
- [GitLab BV (Belgium)](/handbook/benefits/bv-benefits-belgium)
- [GitLab Lyra (India)](/handbook/benefits/lyra-benefits-india)
- [GitLab Inc (US)](/handbook/benefits/inc-benefits-us)
- [GitLab Inc (China)](/handbook/benefits/inc-benefits-china)
- [GitLab LTD (UK)](/handbook/benefits/ltd-benefits-uk)
- [GitLab GmbH (Germany)](/handbook/benefits/gmbh-benefits-germany)
- [GitLab PTY (Australia)](/handbook/benefits/pty-benefits-australia)
- [Safeguard](/handbook/benefits/safeguard/)
- [CXC](/handbook/benefits/CXC)
- Contractors of GitLab BV are eligible for the [general benefits](/handbook/benefits/#general-benefits), but are not eligible for entity specific benefits as they have an additional 17% added to the [compensation calculator](/handbook/people-group/global-compensation/#compensation-calculator). A contractor may bear the costs of their own health insurance, social security taxes and so forth, leading to a 17% higher compensation for the contractor within the calculator.

## Guiding Principles

These principles will guide our benefit strategies and decisions.

* **Collaboration**
  - Work with providers, vendors, and global networks to benchmark benefit data from other similar sized companies.
  - Foster cross-company understanding.
* **Results**
  - Evangelize benefit programs in each entity.
  - Transform “statutory” to “competitive.”
  - Make benefits a very real aspect of compensation during the hiring process.
  - Measure employee engagement and benefit enrollment.
* **Efficiency**
  - Use the GitLab Inc (US) benefits as a starting point, and try to stay as consistent as possible within the US baseline.
  - Iterate on changes for specific countries based on common practices and statutory regulations.
* **Diversity & Inclusion**
  - Actively work to ensure that our benefit choices are inclusive of all team members.
* **Iteration**
  - Regularly review current benefits (bi-annually) and propose changes.
  - Everything is always in draft.
* **Transparency**
  - Announce and document benefits to keep them updated.
  - Share upcoming benefit plans internally before implementing them.
  - Invite discussion and feedback.

We value opinions but ultimately People Operations/Leadership will make the decision based on expert advice and data.

### Guiding Principles in Practice 

When establishing a new co-employer or entity we will outline the following benefits as to why it is or is not offered. 

1. Medical
1. Pension
1. Life Insurance 

We do not have specific budgets around benefit costs, but instead look to increasing the ability to recruit and retain team members in favorable locations. We do not take a global approach to offering the same benefits in all countries, but will transparently outline why we do or do not offer the above benefits on their respective [entity specific benefits](/handbook/benefits/#entity-specific-benefits) page. 

To ensure we have this documented for all countries we currently have an entity or co-employer, we will review the following in order of priority: 
1. Germany
1. Ireland
1. UK
1. Netherlands
1. Belgium 
1. India 
1. Canada: [Completed](/handbook/benefits/CXC/#canada)
1. China
1. Australia
1. Spain
1. New Zealand
1. US: [Completed](/handbook/benefits/inc-benefits-us/)

## General Benefits

For the avoidance of doubt, the benefits listed below in the General Benefits section are available to contractors and employees, unless otherwise stated. Other benefits are listed by countries that GitLab has established an entity or co-employer and therefore are applicable to employees in those countries only. GitLab has also made provisions for Parental Leave which may apply to employees and contractors but this may vary depending on local country laws. If you are unsure please reach out to the compensation team.

1. GitLab will pay for the items listed under [spending company money](/handbook/spending-company-money).
1. [Stock options](/handbook/stock-options/) are offered to most GitLab team-members.
1.  Deceased team member:
    In the unfortunate event that a GitLab team-member passes away, GitLab will
    provide a [$20,000](/handbook/people-group/global-compensation/#exchange-rates) lump sum to anyone of their choosing. This can be a spouse,
    partner, family member, friend, or charity.
      * For US based employees of GitLab Inc., this benefit is replaced by the
        [Basic Life Insurance](/handbook/benefits/inc-benefits-us/#basic-life-insurance-and-add).
      * For all other GitLab team-members, the following conditions apply:
         * The team member must be either an employee or direct contractor.
         * The team member must have indicated in writing to whom the money
           should be transferred. To do this you must complete this [expression of wishes](https://docs.google.com/document/d/1bBX6Mn5JhYuQpCXgM4mkx1BbTit59l0hD2WQiY7Or9E/edit?usp=sharing) form, email to the people ops, who will then file in BambooHR.
         * For part-time GitLab team-members, the lump sum is calculated pro-rata, so
           for example for a team member that works for GitLab 50% of the time,
           the lump sum would be [$10,000](/handbook/people-group/global-compensation/#exchange-rates).
1. [Paid time off policy](/handbook/paid-time-off)
1. [Tuition Reimbursement](/handbook/people-group/code-of-conduct/#tuition-reimbursement)
1. [GitLab Contribute](/company/culture/contribute)
   * Every nine months or so GitLab team-members gather at an exciting new location to [stay connected](/blog/2016/12/05/how-we-stay-connected-as-a-remote-company/), at what we like to call GitLab Contribute. It is important to spend time face to face to get to know your team and, if possible, meet everyone who has also bought into the company vision. There are fun activities planned by our GitLab Contribute Experts, work time, and presentations from different functional groups to make this an experience that you are unlikely to forget! Attendance is optional, but encouraged. For more information and compilations of our past events check out our [previous Contributes (formerly called GitLab Summit)](/company/culture/contribute/previous).
1. [Business Travel Accident Policy](https://drive.google.com/a/gitlab.com/file/d/0B4eFM43gu7VPVl9rYW4tXzIyeUlMR0hidWIzNk1sZjJyLUhB/view?usp=sharing)
   * This policy provides coverage for team members who travel domestic and internationally for business purposes. This policy will provide Emergency Medical and Life Insurance coverage should an emergency happen while you are traveling. In accompaniment, there is coverage for security evacuations, as well a travel assistance line which helps with pre-trip planning and finding contracted facilities worldwide.
   * Coverage:
      - Accidental Death [enhanced coverage]: 5 times Annual Salary up to USD 500,000.
      - Out of Country Emergency Medical: Coverage up to $250,000 per occurrence. If there is an injury or sickness while outside of his or her own country that requires treatment by a physician.
      - Security Evacuation with Natural Disaster: If an occurrence takes place outside of his or her home country and Security Evacuation is required, you will be transported to the nearest place of safety.
      - Personal Deviation: Coverage above is extended if personal travel is added on to a business trip. Coverage will be provided for 25% of length of the business trip.
      - Trip Duration: Coverage provided for trips less than 180 days.
      - Baggage & Personal Effects Benefit: $500 lost bag coverage up to 5 bags.
   * For any assistance with claims or questions, please contact the [People Operations Analyst](/company/team/#brittany_rohde).
   * This policy will not work in conjunction with another personal accident policy as the Business Travel Accident Policy will be viewed as primary and will pay first.
1. [Immigration](/handbook/people-group/visas/) Benefits for eligible team members.
1. [Employee Assistance Program](/handbook/benefits/#employee-assistance-program)
1. [Incentives](/handbook/incentives) such as
   - [Sales Target Dinner Evangelism Reward](/handbook/incentives/#sales-target-dinner)
   - [Discretionary Bonuses](/handbook/incentives/#discretionary-bonuses)
   - [Referral Bonuses](/handbook/incentives/#referral-bonuses)
   - [Visiting Grant](/handbook/incentives/#visiting-grant)

## All-Remote

GitLab is an [all-remote](/blog/2018/10/18/the-case-for-all-remote-companies/) company; you are welcome to [read our stories](/company/culture/all-remote/stories/) about how working remotely has changed our lives for the better.

You can find more details on the [All Remote](/company/culture/all-remote/) page of our handbook.

_If you are already a GitLab employee and would like to share your story, simply add a `remote_story:` element to your entry in `team.yml` and it will appear
on that page._

## Parental Leave

Anyone (regardless of gender) at GitLab who becomes a parent through childbirth or adoption is able to take fully paid parental leave. GitLab team-members will be encouraged to decide for themselves the appropriate amount of time to take and how to take it. We will offer everyone who has been at GitLab for a year up to 12 weeks of 100% paid time off during the first year of parenthood. We encourage parents to take 8-12 weeks.

For many reasons, a team member may require more time off for parental leave. Many GitLab team-members are from countries that have longer standard parental leaves, occasionally births have complications, and sometimes 12 weeks just isn't enough. Any GitLab team-member can request additional unpaid parental leave, up to 4 weeks. We are happy to address anyone with additional leave requests on a one-on-one basis. All of the parental leave should be taken in the first year.

If you have been at GitLab for a year your parental leave is fully paid. If you've been at GitLab for less than a year it depends on your jurisdiction. If applicable, commissions are paid while on parental leave based on the prior six months of performance with a cap at 100% of plan. For example, if in the six months prior to starting parental leave you attained 85% of plan, you will be compensated at the same rate while on leave. On the day you return from leave and going forward, your commissions will be based on current performance only.

You are entitled to and need to comply with your local regulations. They override our policy.

**How to Apply for Parental Leave**

1. To initiate your parental leave, please send an email to People Ops analysts and your manager at least thirty days before your leave will begin, and also take a look at the process for application based on your employment company Country Specific Leave Requirements, which are subject to change as legal requirements change:
  * [GitLab Inc. United States Leave Policy](/handbook/benefits/inc-benefits-us/#gitlab-inc-united-states-leave-policy)
  * [GitLab Inc. China Leave Policy](/handbook/benefits/inc-benefits-china/#gitlab-inc-china-leave-policy)
  * [GitLab B.V. Netherlands Leave Policy](/handbook/benefits/bv-benefits-netherlands/#gitlab-bv-netherlands-leave-policy)
  * [GitLab B.V. Belgium Leave Policy](/handbook/benefits/bv-benefits-belgium/#gitlab-bv-belgium-leave-policy)
  * [GitLab B.V. India Leave Policy](/handbook/benefits/lyra-benefits-india/#gitlab-bv-india-leave-policy)
  * [GitLab LTD United Kingdom Leave Policy](/handbook/benefits/ltd-benefits-uk/#gitlab-ltd-united-kingdom-leave-policy)
  * [GitLab GmbH Germany Leave Policy](/handbook/benefits/gmbh-benefits-germany/#gitlab-gmbh-germany-leave-policy)
  * [GitLab PTY Australia Leave Policy](/handbook/benefits/pty-benefits-australia/#gitlab-pty-australia-leave-policy)
1. People ops analyst will log and monitor upcoming parental leave in the "Parental Leave" Google sheet on the drive.
1. Once the leave is official, People Ops analysts will file all documentation in BambooHR and update the status of the team member to "Parental Leave" in Employment Status table. The date entered is the date the team member started leave. An additional entry should be made with the date the team member will return with the status "Active."
  * This will generate an email to People Ops to remind them to end the leave in any applicable payroll systems.

If you need any additional leave, please refer to our [Paid Time Off Policy](/handbook/paid-time-off).

If you're interested in learning about how other GitLab team-members approach parenthood, take a look at [the parenting resources wiki page](https://gitlab.com/gitlab-com/gitlab-team-member-resources/wikis/parenting) and [#intheparenthood](https://gitlab.slack.com/messages/CHADS8G12/) on Slack.

## Employee Assistance Program

GitLab offers an an Employee Assistance Program to all team members via [Modern Health](https://www.joinmodernhealth.com/). Modern Health provides technology and professional support to help reduce stress, feel more engaged, and be happier. Through GitLab, you have access to coaching sessions at no cost to you.

### What does Modern Health offer?

Modern Health is the one-stop shop for all tools related to mental well-being and self-improvement. Members gain access to the following features:
* **Personalized Plan:** Take a well-being assessment and review which tools may be most helpful for you.
* **Professional Support** Get matched to a dedicated coach or therapist who can help you reach your personal and professional goals. Coaching visits are covered by GitLab and are offered at no cost to you.
* **Evidence-Based Digital Care:** Develop a toolkit of mental hacks through online courses that take up less than 15 minutes per week.
* **Curated Content Library:** Learn more quick tips and tricks to prevent burnout, manage stress, and cope with anxiety or depression.

If you’re still not sure where to get started, we recommend that you
1. Register and take the well-being assessment and
1. Get matched to a dedicated coach who can work with you to figure out next steps.

### Which areas does Modern Health support?

Modern Health cultivates the resilience needed to weather the ups and downs of everyday life. Here are the specific areas where we can help:
* Work Performance: Productivity, Leadership Skills, Work Relationships, Professional Development
* Relationships: Romantic Relationships & Dating, Family, Friends, Breakups
* Stress & Anxiety: Anxiety, Depression, Stress, Resilience
* Healthy Lifestyles: Sleep, Physical Activity, Eating Well, Habits
* Financial Well-being: Goals, Budgeting Savings and Debt, Management, Investing
* Diversity & Inclusion: Gender, Equality, Unconscious Bias, LGBTQ
* Life Challenges: Pregnancy/Parenting, Elder/Child Care, Loss of a Loved One, Illness
* Mindfulness & Meditation: Stress Less, Sleep Better, Focus Better, Meditation for Beginners

Note: This list isn’t intended to be comprehensive. Please reach out to `help@joinmodernhealth.com` with any questions about how or where to get started.

### How does Modern Health think about mental health?

The philosophy towards mental health comes from the World Health Organization (WHO): “in which every individual realizes his or her own potential, can cope with the normal stresses of life, can work productively and fruitfully, and is able to make a contribution to her or his community.”

### When do my Modern Health benefits reset?

Your benefits reset 1 year from the launch date, on May 15th, 2020.

### What languages is Modern Health available in?

The platform is currently available in English and Spanish. If English or Spanish is not your preferred language, please note Care is provided in most languages. Feel free to email `help@joinmodernhealth.com` to find out more.

### Registration

* Am I eligible? All GitLab team-members are eligible for Modern Health.
* How do I register?
  * Download the Modern Health app in the [Google Play Store (Android)](https://play.google.com/store/apps/details?id=com.modernhealth.modernhealth) or [App Store (iOS)](https://itunes.apple.com/us/app/modern-health/id1445843859?ls=1&mt=8).
  * After your download is complete, select “Join Now” from the welcome page of the mobile app.  
  * Use the first and last name you have on file with GitLab.
  * Verify your date of birth (DOB) and company code (GitLab).
  * Enter the your GitLab email and password of your choice. (Reminder that you must register with your GitLab email, but you can change your Modern Health account email to your personal email in Settings upon registration.)
  * Select “Register” on the web or “Agree & Join” on the mobile app to complete registration.

If you have trouble registering for Modern Health, please don’t hesitate to reach out to `help@joinmodernhealth.com` with a note or screenshot. Their customer support team will verify the information (i.e., first name, last name, DOB, and company code) against what they have on file to provide you the best instructions on how to successfully access Modern Health.

### What is the well-being survey, and why should I take it?

Similar to an annual physical with your primary care physician, Modern Health’s well-being survey serves as a check up for your mental health. You can retake the survey to track your progress overtime.

Your well-being score empowers experts at Modern Health to provide you the best user experience. It enhances the customization of your personalized wellness plan, which makes it more effective in addressing your specific needs. Although the ups and downs in well-being score are inevitable, the data-driven approach keeps up with how you’re doing over time to support you with the tools to improve no matter where your score is at today.  

### Care

Modern Health has several different types of providers that you can work with. They have both therapists and coaches. Here is a breakdown of the different types of providers.
1. Coach
  * Coaches work with individuals to help them have the kind of lives they want. While there is no specific degree or license required to practice as a coach, all Modern Health coaches are rigorously vetted, certified, and trained in evidence-based approaches.  
1. Therapist
  * Therapists can have a variety of titles (counselors, therapists, clinicians, etc.). What they have in common is a masters degree (M.A., or M.S.) in clinical psychology or a related field and are licensed in the state in which they practice. Common licenses include Licensed Marriage and Family Therapist (LMFT), Licensed Clinical Social Worker (LCSW), and Licensed Professional Counselor (LPC). These therapists are also trained in the assessment and treatment of mental health concerns.
1. Psychologist
  * Psychologists have a doctoral degree (Ph.D., or Psy.D.) in clinical psychology or a related field such as counseling psychology or education and are licensed in the state in which they practice. They are trained in the assessment and treatment of mental health concerns.

**What is the difference between coaching and therapy?**
The primary difference between coaching and therapy is that therapy is conducted by licensed mental health professionals who are trained to treat clinical difficulties (e.g., depression, anxiety) whereas coaches work on non-clinical issues (e.g, personal growth, managing stress, relationships, professional development, etc). Modern Health’s belief is that anyone can benefit from working with a coach, and some people need therapy in addition to or instead of coaching. If you are experiencing a clinical need Modern Health will recommend a therapist. You can also work with your dedicated coach to determine if you would benefit from therapy.

**How do you match me to a provider?**
Modern Health matches you to a provider based on a proprietary algorithm that weights goodness of fit, including your well-being score and areas you want to work on.

**Can I complete sessions with my partner or a family member?**
You are able to complete sessions with your partner or family member, however in most cases this will count as extra sessions (e.g, 1 session with a partner is equivalent to 2 sessions with you alone). Please discuss this directly with your coach.

**What happens if my provider isn’t a good fit?**
The goal at Modern Health is to find someone that you feel you can do good work with and who can do good work with you! If you think the first person you meet with doesn’t seem like a good fit, just let them know (`help@joinmodernhealth.com`) and they will connect you with someone new.

**What is coaching?**
Coaching is a collaborative process to help you make important changes in your personal and professional life. Your coach is there to help you figure out how you want to change and the steps you need to take to do so. Your coach’s job is to help you organize your thoughts, emotions, and goals and break things down into smaller steps that create forward movement and growth. The individual is the driver of these sessions, the coach is there to provide reflection, clarity, and accountability.

**How often should I meet my coach or therapist?**
How often you meet with your coach depends on your personal situation. Some people like to meet weekly, whereas others meet every month or two. Usually people will meet with their coach every two weeks.

### Logistics

* You have virtual coaching sessions covered by GitLab.  
* Your coach will let you know how many sessions are remaining with your plan.
* If you are experiencing a clinical need, Modern Health will recommend a therapist to work with in addition to/instead of your coach. You can also work with your dedicated coach to determine if you would benefit from therapy.
* You have unlimited access to your coach over text and/or email.
* Sessions are held over video/phone.
* Cancellation/No Show
  * If you need to miss a scheduled session please let your coach/therapist know at least 24 hours before your appointment. If you cancel after that time, or miss the session, it will count towards your total covered sessions.
* What happens when I finish my covered Modern Health sessions?
  * As you are approaching the end of your covered Modern Health sessions, we encourage you and your coach to have a discussion about whether you want to wrap up after the covered sessions, or if you want to continue. If you choose to continue, you will be able to do so at a Modern Health negotiated rate and will pay your coach/therapist directly.

### Confidentiality

All information submitted through the Modern Health application is kept confidential and used to deliver a more personalized experience. Anonymized group aggregate information is served back to GitLab for additional insights to improve a tailored care plan.

Modern Health employs industry-standard Secure Socket Layer (SSL) and Hypertext Transfer Protocol Secure (HTTPS) encryption measures for all data exchanged between our members and our application. For more information, please refer to our Privacy Policy.

### Crisis Support

**What do I do in a crisis?**
If you are experiencing a crisis (e.g., thoughts about suicide, thoughts about harming yourself or others, medical crisis, or in a dangerous situation) please call:
* emergency responders (911 or the [appropriate local emergency number for your area](https://en.wikipedia.org/wiki/List_of_emergency_telephone_numbers))
* in the US/Canada, https://suicidepreventionlifeline.org/ or 1-800-273-8255) or [a crisis line in your area](https://en.wikipedia.org/wiki/List_of_suicide_crisis_lines)
* head to the nearest emergency room.

### Modern Health Weekly Reporting

* Pull the Modern Health BambooHR report
* Sort by hire date, remove anyone who hasn't started and copy the report
* Open the `Modern Health Upload` spreadsheet on the google drive and paste the report under a new tab
* Rename the tab to today's date
* Download as a CSV file
* Email the file the secure upload using Box 


## Global Benefits Survey

We've partnered with Culture Amp to run our Global Benefits Survey, which will launch on June 10th. We are running this survey to collect feedback from all team members, which will be collected, aggregated, and analyzed. Using Culture Amp's reporting tools, we'll analyze the data to find the most common trends for desired benefit iterations. We will use this information to generate a roadmap and share these results with everyone at GitLab.

This is an opportunity to share with the compensation team what benefits you think are great, what benefits are lacking, and what you think is most important to adjust. While we will review and prioritize all feedback, not all input may be implemented in the next few iterations. We will continue to ask the organization what is important to team members, but also take into account our fiduciary responsibility to maintain our operating costs.

**Timeline of the Survey Results:**
* 2019-06-10 Survey goes out to team members: DONE
* 2019-06-21 Survey closes: DONE
* 2019-06-24 to 2019-07-05 Survey results analyzed: DONE
* 2019-07-12 Survey Results Shared with the GitLab Team: DONE
* 2019-07-15 to 2019-07-26 Survey Results issues opened to begin implementation of any changes.
* Q3 OKR to implement changes based on survey results viewed in Q2.

Thank you again for playing a part in our efforts to continually improve total rewards at GitLab. If you have any questions, please reach out to the compensation team via email.

### Global Benefits Survey Results

Participation: 62%

**Rating Rubric**
5 - strongly agree
4 - agree
3 - neutral
2 - disagree
1 - strongly disagree

**Rating Rubric Results:**

1. I Understand the Benefits Package
  * | GitLab                 | Score |
    |------------------------|-------|   
    | Male	                 | 4.16  |
    | Female                 | 4.21  |
    | Individual Contributor | 4.13  |
    | Manager                | 4.35  |
    | Leader                 | 4.46  |
    | Company Overall        | 4.18  |
1. The general benefits at GitLab are equal to or better than what is offered by similar employers
  * | GitLab                 | Score |
    |------------------------|-------|   
    | Male	                 | 3.67  |
    | Female                 | 3.66  |
    | Individual Contributor | 3.72  |
    | Manager                | 3.33  |
    | Leader                 | 3.46  |
    | Company Overall        | 3.66  |
1. I believe my benefits package is equal to or better than what is offered by similar employers
  * | GitLab                 | Score |
    |------------------------|-------|   
    | Male	                 | 3.44  |
    | Female                 | 3.50  |
    | Individual Contributor | 3.54  |
    | Manager                | 3.00  |
    | Leader                 | 3.27  |
    | Company Overall        | 3.46  |
1. The general benefits at GitLab save me a great deal of time and/or money, and add significant value to my employee experience
  * | GitLab                 | Score |
    |------------------------|-------|   
    | Male	                 | 3.75  |
    | Female                 | 3.79  |
    | Individual Contributor | 3.83  |
    | Manager                | 3.38  |
    | Leader                 | 3.54  |
    | Company Overall        | 3.77  |
1. My benefits package provides quality coverage for myself and, if applicable, my dependents
  * | GitLab                 | Score |
    |------------------------|-------|   
    | Male	                 | 3.59  |
    | Female                 | 3.63  |
    | Individual Contributor | 3.63  |
    | Manager                | 3.38  |
    | Leader                 | 3.58  |
    | Company Overall        | 3.60  |
1. The wellness offerings at GitLab help me lead a happier, healthier life
  * | GitLab                 | Score |
    |------------------------|-------|   
    | Male	                 | 3.39  |
    | Female                 | 3.28  |
    | Individual Contributor | 3.41  |
    | Manager                | 3.26  |
    | Leader                 | 2.92  |
    | Company Overall        | 3.36  |
1. I believe our benefits package is one of the top reasons why people apply
  * | GitLab                 | Score |
    |------------------------|-------|   
    | Male	                 | 2.87  |
    | Female                 | 2.81  |
    | Individual Contributor | 2.96  |
    | Manager                | 2.40  |
    | Leader                 | 2.35  |
    | Company Overall        | 2.86  |
1. Should I have or care for a(nother) child, the parental leave policy is sufficient	 
  * | GitLab                 | Score |
    |------------------------|-------|   
    | Male	                 | 3.95  |
    | Female                 | 3.44  |
    | Individual Contributor | 3.82  |
    | Manager                | 3.61  |
    | Leader                 | 4.04  |
    | Company Overall        | 3.81  |
1. The vacation policy allows me sufficient time to recharge
  * | GitLab                 | Score |
    |------------------------|-------|   
    | Male	                 | 4.48  |
    | Female                 | 4.40  |
    | Individual Contributor | 4.49  |
    | Manager                | 4.27  |
    | Leader                 | 4.38  |
    | Company Overall        | 4.46  |
1. I believe our benefits package is one of the top reasons why people stay at GitLab
  * | GitLab                 | Score |
    |------------------------|-------|   
    | Male	                 | 3.08  |
    | Female                 | 3.13  |
    | Individual Contributor | 3.21  |
    | Manager                | 2.58  |
    | Leader                 | 2.54  |
    | Company Overall        | 3.09  |
1. I believe investing more of the company's money into improving benefits will help attract and retain top talent
  * | GitLab                 | Score |
    |------------------------|-------|   
    | Male	                 | 4.16  |
    | Female                 | 4.25  |
    | Individual Contributor | 4.20  |
    | Manager                | 4.25  |
    | Leader                 | 3.85  |
    | Company Overall        | 4.18  |

**Comment Responses:**

Please note this is a summary of suggestions based on aggregated global team member feedback, but are not guaranteed to be implemented or adjusted at GitLab.

1. What provider for benefits would you prefer? (Entity/PEO where we have data)
  * Australia: BUPA, HCF, NIB, AHM
  * Canada: Great West Life, Blue Cross, Manulife, Sunlife
  * India: ICICI Lombard, Apollo Munich, Bajaj Allianz
  * Ireland: VHI. Laya, Irish Life
  * Netherlands: UMC Zorgverzekering, VGZ, ABP
  * United Kingdom: Virgin Active, BUPA, AVIVA, AXA PPP
  * United States: 45% of respondents commented, 21% UHC, 14% BCBS, 3% Kaiser, 2% Cigna
1. What general benefit do you think would be the best new addition to the company's offering to implement?
  * Wellness program (gym, fitness, etc) (overwhelming support for this)
  * Global insurance programs to align with US/UK
  * Pension with company match globally
  * Company Annual Bonus or extra month pay for vacation bonus
  * Charitable Giving
  * Team Level Contribute
  * Other Benefits: Loan Assistance, Childcare, Pet Insurance, Food Allowance
1. What benefits would you want GitLab to retain?
  * Unlimited Paid Time Off
  * Parental Leave
  * Stock Options
  * Tuition Reimbursement
  * Contribute
  * Remote Work
  * Travel Stipend/Visiting Grant
  * Employee Assistance Program
  * All Benefits
1. What benefits would be most willing to sacrifice to allow for new benefits?
  * Sales Incentive Dinner
  * Stock Options
  * Employee Assistance Program
  * Tuition Reimbursement
  * Visiting Grant
  * Referral/Discretionary Bonus
  * Travel Accident Insurance

**Action Items/Issues to Open:**

Please note this is the first iteration/more immediate goals, but this section will be updated as we progress through the project.

* Clarify what Benefits Contractors are eligible for: [Merge Request](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26491/diffs)
* Ensure all PEO benefits are documented on the respective page: TODO 
* Benchmark against other remote companies/survey data to prioritize new benefits to offer/replace: TODO 
* Review Implementation of a Wellness Program (Gym/Fitness): [Issue](https://gitlab.com/gitlab-com/people-ops/Compensation/issues/37)
* Review Global Health Benefits, Pension, Life Insurance: TODO 
* Establish a benefits budget with Finance to implement new benefits: TODO 
* Review all PEO benefits to ensure they align to our global benefits principles: TODO 

## Benefits Not Currently Being Implemented

This section serves to highlight benefits that we have previously researched, but have decided not to move forward with implementing at this time. As the company grows, circumstances change, and/or we receive new information, we may decide to revisit any benefits added to this list.

**TeleHealth**

We researched and selected four vendors to receive more information. Demo calls were conducted with three of these vendors where we learned more about the solutions and pricing. After reviewing the [results of the benefits survey](/handbook/benefits/#global-benefits-survey-results), there wasn’t enough interest in a telehealth solution to justify the price so we decided to not move forward with any of the vendors at this time.

Further information and corresponding discussion are available under [Compensation Issue #15](https://gitlab.com/gitlab-com/people-ops/Compensation/issues/15) (internal).

**Coworking Space Agreement**

We researched whether there was an account that could be set up with a coworking space chain that would make it easier for team members to utilize these spaces. Three major chains with global coverage were contacted with one responding. After conducting a survey, the type of account this chain offers doesn't align with the way the majority of team members utilize coworking spaces. Team members are still welcome to follow the existing process of [expensing a coworking space](/handbook/spending-company-money/).

Further information, survey results, and corresponding discussion are available under [Compensation Issue #30](https://gitlab.com/gitlab-com/people-ops/Compensation/issues/30) (internal).