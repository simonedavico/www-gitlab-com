---
layout: markdown_page
title: "Payroll"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

This page is a stub. It will be expanded soon.

## US

## Non-US

### Legal Entity

### Contractors

### PEO

## Expenses

## Performance Indicators

### Payroll accuracy for each check date = 100%
Payroll is paid on time and accurately for each check date. 

### Payroll journal entry reports submitted to Accounting <= Payroll date + 2 business days
Payroll journal entry reports are to be submitted to Accounting no later than two business days after the payroll date. The payroll journal entry reports submitted dates are tracked in the Monthly Closing Checklist on a monthly basis. 
