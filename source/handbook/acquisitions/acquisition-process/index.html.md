---
layout: markdown_page
title: "Acquisition Process"
---

## On this page
{:.no_toc}

- TOC
{:toc}

This is a detailed view of our acquisition process. For more information about
our acquisitions approach visit our [acquisitions handbook](/handbook/acquisitions).

### Acquisitions deal flow
Below is the deal flow of the acquisitions process with estimated conversion rates and metrics for each stage we think there can be a drop point. At this point, the estimates are hypothetical and not based on actual deal flow data we've collected through our process. The flow aims to capture the success rate and volume of the acquisitions process for GitLab.

| Step | Success rate/volume |
| --- | ---|
| Potential target companies | 1000/year |
| Strategy fit | 30% |
| Deal term fit | 15% |
| NDA | 100/year |
| Code screen share | 50% |
| Founders technical interview | 70% |
| Resume review of all people | 70% |
| *Optional step*: sample interviews with non-founders (will increase LOI success rate) | |
| Deal terms discussed and socialized |
| LOI | 15/year |
| Review all code | 90% |
| Interview all people | 60-90% |
| Acquisition offer | 12/year |
| Acquisition closed | 10/year |

## Acquisition process
The process is comprised of three key stages:
1. Exploratory
1. Business case
1. Diligence

### Exploratory stage
1. Intro call: we'll reach out to schedule a 50 minute introductory call. The purpose of the call is to:
    1. Current state of your company including team, products, financials, and more
    1. Review the expectations and process noted on this page
    1. Start discussing which features could be built into gitlab
    1. Discuss which GitLab product category the team could join as a whole    
    1. Discuss deal terms as noted on the [acquisitions handbook](/handbook/acquisitions)
    1. Answer questions your team may have
Details from this call should be collected following the [Initial Acquisition Review Template](https://docs.google.com/document/d/1RekjfQj89pIV7DZzeNZ00HYOwqlqlDFm7Gy4yZET9rc/edit?usp=sharing)(a GitLab internal document).
TARGET TEAM: Ahead of the product call please review our [roadmap](/direction/) and outline which of your current and future product features can be implemented into GitLab's product categories. Outline a simple integration timeline for those features, considering an MVC release on the first month after joining GitLab and monthly releases following with quick iterations.
1. Mutual NDA: Sign a mutual NDA as linked on our [legal handbook page](/handbook/legal/)
1. Create a new, private Slack channel for the acquisition team discussions. Format: `#acq-company_name`. Add VP Product Strategy and where relevant, the relevant product category director/s to newly created Slack channel.
1. Add template WIP Business Case to the top of the acquisition gdoc and start filling the details
1. Product call/s: start product diligence and deep dive into the discussion of which features could be built into GitLab and into which GitLab product stage. Discuss strategy fit to GitLab's [product roadmap](/direction/).
1. Internal review: validate potential fit for the team within GitLab and the integration options into GitLab
1. [Form the acquisition team](/handbook/acquisition-process/#forming_an_acquisition_team)
1. Preliminary financial & legal diligence - list of preliminary documents to share with GitLab:
   1. Financials
   1. Tax returns
   1. Employee roster with: employee name, title, role, tenure, years of experience, location, salary, LinkedIn profile, programming languages proficiency
   1. Employee Résumés and/or LinkedIn profiles
   1. Employee agreements and PIAA
   1. Customer list with name, monthly revenue, contract termination date and any other fields if relevant.
   1. Vendor list with monthly spend
   1. Asset list
   1. Any assets that are needed for the business and will be part of the acquisition
   1. Assets excluded from the acquisition
1. Early technical diligence:
   1. In case the target company has open source components, the respective Dir. Engineering (dependent on GitLab stage) will start an early code review to determine: code quality, development practices, contributions and more. That should be turned around within 2-3 business days.
   1. Hands-on product and code screen-share session (2 hours): the technical lead, as assigned by the respective Dir. Engineering, together with the respective Dir. Product will lead a screen-share session aimed at a hands-on validation of the product functionalities and an overview of the code.
1. Founder technical interviews - founders will go through two rounds of interviews to assess technical and cultural alignment.
1. Résumé review - Review of all employee résumés
1. Optional interviews for a sample of the technical employees - to increase the success rate of the deal post-LOI we recommend conducting interviews for a sample of the technical employees before we sign the LOI. This will reduce the element of uncovering gaps during the diligence stage. The interviews will include a technical interview and a manager interview as detailed in the diligence stage below.

### Business case stage
1. Product integration strategy: the lead PM will formalize the integration strategy with a focus on feature sets/functionalities:
   1. What we keep as-is
   1. What we reimplement in GitLab
   1. What we discard/EOL
   1. Critical for user migration
   1. Target [product tiers](/handbook/marketing/product-marketing/tiers/) in GitLab
   1. Barriers for implementation
1. Present business case for internal review of the acquisition proposal.
1. LOI: Using the [LOI template](https://docs.google.com/document/d/1bSnGDPS94BGd8C9MfUvJA34X-AcpVp7wDekcBIL_Adw/edit)(_internal_ GitLab document) and assuming all terms have been reviewed, the acquisition lead will share a LOI within 7 days.

### Diligence stage
1. Technical diligence:
   1. Code diligence - your team will provide access for key engineering contacts at GitLab to: code repositories, products and development/test environments.
   1. Employee evaluations - each engineer will undergo two rounds of interviews comprised of:
      1. Technical assessment - a live coding session, solving a technical exercise or review a merge request with live followup session.
      1. Manager interview
  In case of acquisitions with over 10 engineers the employee evaluation stage
  will happen in phased approach for 5 employees at a time.
1. Financial & legal diligence
1. CEO call: Once integration of the product and team have been scoped out the acquisition lead will reach out to the CEO PA to schedule a 50 minute call. The purpose of this call is to:
    1. Discuss the wind down and onboarding process details including roles for the founders and the placement of the team within GitLab.
    1. Review the employee onboarding goals


## Forming an acquisition team
An acquisition team will consist of the following GitLab functions:
1. Acquisition lead, currently Eliran Mesika on all acquisitions
1. VP Product Strategy
1. Dir. Engineering
1. Finance/accounting team member, currently Paul Machle
1. Legal team member, currently Jamie Hurewitz

To assign the product manager, after the product call or as soon as it's clear which product category the features will be implemented into, contact the category product director for the assignment.

To assign the engineer team member, contact the engineering manager of the relevant category for assignment.

### Acquisition team responsibilities

| Member | Role | Deliverables |
| -- | -- | -- |
| Acquisition lead | 1. Main POC for acquired team 1. Identify potential areas for integration 1. Create case for acquisition and customer transition story | 1. Business case with deal structure |
| VP Product Strategy | 1. Outline current product features to be implemented into GitLab 1. Outline potential future functionalities to be built into GitLab after the integration period | 1. Integration strategy|
| Dir. Engineering | 1. Lead technical diligence | 1. Code quality review 1. Integration strategy validation - feasibility and timeline |
| Finance | 1. Lead financial diligence 1. Validate business case and deal structure | |
| Legal | 1. Review entity, assets and existing agreements 1. Evaluate sunset and customer transition path | 1. LOI 1. Acquisition agreement|

## Acquisitions are confidential
At GitLab, we treat acquisitions as confidential and we share the work on them on a need-to-know basis. If you're part of an acquisition Slack channel, GDoc or other internal GitLab discussion and would like to invite another GitLab team-member to one of those, please confirm with the acquisition lead before doing so.
