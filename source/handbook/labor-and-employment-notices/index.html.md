---
layout: markdown_page
title: "Labor and Employment Notices"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

Since GitLab is an [all remote](/company/culture/all-remote/) company we don't have a physical worksite or breakroom wall upon which to post important labor and employment notices, so this page is our version of that.

## City of San Francisco

[sf-minimum-wage-2019-06.pdf](https://gitlab.com/gitlab-com/people-ops/Compensation/uploads/e261b0f24e1b2d5bf382366bb400cf53/sf-minimum-wage-2019-06.pdf) - San Francisco Minimum Wage increases to $15.59 per hour.