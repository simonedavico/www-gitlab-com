---
layout: markdown_page
title: "Sales & Customer Enablement"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Welcome to the Sales & Customer Enablement Handbook

**CHARTER**

Ensure everyone in the GitLab sales, partner, and customer ecosystem has the right skills, knowledge, and resources to meet and exceed desired business objectives in accordance with Gitlab’s values.

**KEY PROGRAMS**

*  [Sales Onboarding](/handbook/sales/onboarding/)
*  [GitLab Command of the Message](/handbook/marketing/product-marketing/sales-resources/#command-of-the-message)
*  [Continuous Improvement / Continuous Development for Sales (CI/CD for Sales)](/handbook/sales/training/)

**Chat Between David Somers- Director, Sales and Customer Enablement-  and Sid- CEO- on Taking A Handbook-First Approach to GitLab Learning Materials**
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/oXTZQpICxeE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

Key Discussion points:
* Our [Mission](/company/strategy/#mission) is that Everyone Can Contribute, and our most important value is [Results](/handbook/values/#results). Like we've extended that to the Handbook, we want to extend it to our Learning Materials.
* We want to leverage the best of an e-learning platform, with the benefits of reminders, interactivity, and more but make sure the materials we produce are also available to those who aren't using an e-learning platform, while fulfilling [our mission](/company/strategy/#mission). 
* There are benefits to keeping our e-learning material [handbook-first](/handbook/handbook-usage/#why-handbook-first):
   * Folks who have already completed a formal training through an e-learning platform may want to return to the materials.
   * Those who never go through the formal platform may also benefit from the materials. 
   * The handbook continues to be the SSOT, with the e-learning platform leveraging handbook materials through screenshots, embeds, and more. 
