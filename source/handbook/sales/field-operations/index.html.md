---
layout: markdown_page
title: "Field Operations"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Welcome to the Field Operations Handbook

“Manage field business processes, systems, architecture, enablement, champion data integrity, provide insights and predictability through analytics”

### Key Tenants
**Clarity**: for definitions, processes and events   
**Visibility**: to processes, data and analytics   
**Accountability**: for both Sales/Field Ops to uphold to expectations and SLAs   

### Teams
* [Sales Operations](/handbook/sales/field-operations/sales-operations/)
* [Sales Systems](/handbook/sales/field-operations/sales-systems/)
* [Sales Strategy](/handbook/sales/field-operations/sales-strategy/) 
* [Sales & Customer Enablement](/handbook/sales/field-operations/sales-enablement/)