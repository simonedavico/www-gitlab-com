---
layout: markdown_page
title: "Product Vision - Configure"
---

- TOC
{:toc}

This is the product vision for Configure.

- See a high-level
  [roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=✓&state=opened&label_name[]=devops%3A%3Aconfigure).

## Overview

The Configure stage solves user problems related to the configuration and operation of applications
and infrastructure, including features like Auto DevOps, Kubernetes integration, and ChatOps.
We aim to make complex tasks (such as standing up new environments) fast and
easy as well as providing operators all the necessary tools to execute their
day-to-day actions upon their infrastructure.

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/imOtGmDJDCE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Background

In keeping with our [single application](https://about.gitlab.com/handbook/product/single-application/) promise, we want GitLab to be a robust, best-of-breed tool for operators as much as it is for developers. Our vision is that operators will use GitLab as their main day-to-day tool for provisioning, configuring, testing, and decomissioning infrastructure. 

### Challenges

We understand GitLab will play side-by-side with existing tools which teams have already invested considerable time and money. As a result, we will extend GitLab's Configure features to have rich interactions through both the GUI and API. To provide the best experience possible we must identify best-of-breed tools in this segment that share our open source DNA and provide seamless integration.

GitLab is the most popular way to deploy to Kubernetes. Many organizations (of all scales) have adopted Kubernetes prior to their use of our GitLab. We will support this model by identifying the most common set of use cases where Kubernetes clusters exist. We'll then enable those existing clusters to be plugged in to GitLab so users can gain the value of using our integrated development approach. This includes both enterprise users of Kubernetes (typically OpenShift) and startup Kubernetes use (typically public cloud providers).

Our Kubernetes integration currently operates under the assumption of `cluster-admin` privilege. While this is acceptable for most use cases, there is a large minority that, appropriately, operates strictly on least-privilege security principle. In these environments only single-namespace access is allowed and cluster-wide access is denyed. To encourage adoption of these principles, we will provide the same rich integration we provide today for this further restricted use case.

### Opportunities
Our opportunities in the Configure stage are derived from the [Ops Section opportunities](/direction/ops/#opportunities). Namely:
* **IT operations skills gap:** Operations skills are difficult to attract. We'll make it easier for you to leverage your operations expertise by efficiently managing your infrastructure platform. 
* **Clear winner in Kubernetes:** Our focus on enabling cloud-native applications based on the winner in container orchestration and management, Kubernetes, means you can leverage the broader Cloud Native ecosystem as you adopt modern application architectures.

## Performance Indicators (PIs)

Our [Key Performance Indicator](https://about.gitlab.com/handbook/ceo/kpis/) for the Configure stage is the **Configure SMAU** ([stage monthly active users]()). 

Configure SMAU is determined by tracking how users *configure*, *interact*, and *view* the features contained within the stage. The following features are considered:

| Configure                                                                                                                               | Interact                         | View                                                                 |
|-----------------------------------------------------------------------------------------------------------------------------------------|----------------------------------|----------------------------------------------------------------------|
| Create Kubenetes cluster on GKE                                                                                                         | Dismiss Auto DevOps banner       | View cluster health metrics                                          |
| Add existing Kubernetes cluster                                                                                                         | Run Auto DevOps pipeline         | View serverless page                                                 |
| Enable Auto DevOps                                                                                                                      | JupyterHub login                 | View function metrics (/../../../serverless/functions/*/my-function) |
| Install Kubernetes application<br>- Helm<br>- Ingress<br>- Cert Manager<br>- Prometheus<br>- GitLab Runner<br>- JupyterHub<br>- Knative | Upgrade Kubernetes application   | -                                                                    |
| Enable Slack application on gitlab.com                                                                                                  | Uninstall Kubernetes application | -                                                                    |
| Enable Slack slash commands                                                                                                             | -                                | -                                                                    |
| Use Auto DevOps templates (composable Auto DevOps)                                                                                      | -                                | -                                                                    |

See the corresponding [Periscope dashboard](https://app.periscopedata.com/app/gitlab/462967/Configure-Metrics) (internal).

## Auto DevOps

Our vision for “[Auto DevOps](https://www.youtube.com/watch?v=KGrJguM361c)” is
to leverage our [single application](/handbook/product/single-application/) to
assist users in every phase of the development and delivery process,
implementing automatic tasks that can be customized and refined to get the best
fit for their needs.

With the dramatic increase in the number of projects being managed by software
teams (especially with the rise of micro-services), it's no longer enough to
just craft your code. In addition, you must consider all of the other aspects
that will make your project successful, such as tests, quality, security,
logging, monitoring, etc. It's no longer acceptable to add these things only
when they are needed, or when the project becomes popular, or when there's a
problem to address; on the contrary, all of these things should be available at
inception.

e.g. “auto CI” to compile and test software based on best practices for the most
common languages and frameworks, “auto review” with the help of automatic
analysis tools like Code Climate, “auto deploy” based on Review Apps and
incremental rollouts on Kubernetes clusters, and “auto metrics” to collect
statistical data from all the previous steps in order to guarantee performances
and optimization of the whole process. Dependencies and artifacts will be
first-class citizens in this world: everything must be fully reproducible at any
given time, and fully connected as part of the great GitLab experience.

[Watch the video explaining our vision on Auto DevOps](https://www.youtube.com/watch?v=KGrJguM361c)

[Learn more](/product/auto-devops/) • [Documentation](https://docs.gitlab.com/ee/topics/autodevops/) • [Vision](/direction/configure/auto_devops/)

## Kubernetes Configuration

Configuring and managing your Kubernetes clusters can be a complex, time-consuming task. 
We aim to provide a simple way for users to configure their clusters within GitLab; tasks 
such as scaling, adding, and deleting clusters become simple, single-click events.

[Learn more](/solutions/kubernetes/) • [Documentation](https://docs.gitlab.com/ee/user/project/clusters/) • [Vision](/direction/configure/kubernetes_configuration/)

## Serverless

Taking full advantage of the power of the cloud computing model and container
orchestration, cloud native is an innovative way to build and run applications.
A big part of our cloud native strategy is around serverless. Serverless
computing provides an easy way to build highly scalable applications and
services, eliminating the pains of provisioning & maintaining.

[Learn more](/product/serverless/) • [Documentation](https://docs.gitlab.com/ee/user/project/clusters/serverless/) • [Vision](/direction/configure/serverless/)

## Runbook Configuration

[Incident Management](https://gitlab.com/groups/gitlab-org/-/epics/349) will
allow operators to have real-time view into the happenings of their systems.
Building upon this concept, we envision rendering of runbook inside of GitLab as
interactive documents for operators which in turn could trigger automation
defined in `gitlab-ci.yml`.

[Documentation](https://docs.gitlab.com/ee/user/project/clusters/runbooks/) • [Vision](/direction/configure/runbooks/)

## ChatOps

The next generation of our ChatOps implementation will allow users to have a
dedicated interface to configure, invoke, and audit ChatOps actions, doing it in
a secure way through RBAC.

[Documenation](https://docs.gitlab.com/ee/ci/chatops/) • [Vision](/direction/configure/chatops/)

## Infrastructure as Code

Infrastructure as code (IaC) is the practice of managing and provisioning infrastructure through
machine-readable definition files, rather than physical hardware configuration or interactive
configuration tools. The IT infrastructure managed by this comprises both physical equipment
such as bare-metal servers as well as virtual machines and associated configuration resources.
The definitions are stored in a version control system. IaC takes proven coding techniques and 
xtends them to your infrastructure directly, effectively blurring the line between what is an
application and what is the environment.

Our focus will be to provide tight integration with best of breed IaC tools, such that all 
infrastructure related workflows in GitLab are well supported. Our initial focus will be on Terraform.

[Vision](/direction/configure/infrastructure_as_code/)

## Chaos Engineering

Chaos engineering in a powerful practice that allows operators to architect
powerful distributed systems that can withstand turbulent conditions. We want
operators to be able to run downtime scenarios randomly to test the resilience
of their architecture. Starting with the minimum units (pods) all the way the
largest units (regions).

[Vision](/direction/configure/chaos_engineering/)

## Cluster Cost Optimization

Compute costs are a significant expenditure for many companies, whether they
are in the cloud or on-premise. Managing these costs is an important function
for many companies. We aim to provide easy-to-understand analysis of your infrastructure
that could identify overprovisioned infrastructure (leading to waste), recommended changes,
estimated costs, and automatic resizing.

[Vision](/direction/configure/cluster_cost_optimization/)

<%= partial("direction/contribute", :locals => { :stageKey => "configure" }) %>

## Prioritization Process

In general, we follow the same [prioritization guidelines](/handbook/product/#prioritization)
as the product team at large. Issues will tend to flow from having no milestone,
to being added to the backlog, to being added to this page and/or a specific
milestone for delivery.

You can see our entire public backlog for Configure at this
[link](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Configure);
filtering by labels or milestones will allow you to explore. If you find
something you're interested in, you're encouraged to jump into the conversation
and participate. At GitLab, everyone can contribute!

Issues with the "direction" label have been flagged as being particularly
interesting, and are listed in the sections below.

## Upcoming Releases

<%= direction["all"]["all"] %>

<%= partial("direction/other", :locals => { :stage => "configure" }) %>
