---
layout: default
title: Application Security
description: "Effectively turn your DevOps methodology into a DevSecOps methodology"
suppress_header: true
extra_css:
  - devops.css
  - agile-delivery.css
  - application-security.css
extra_js:
  - libs/on-scroll.js
  - in-page-nav.js
  - all-clickable.js
canonical_path: "/application-security/"
---
.blank-header
  %img.image-border.image-border-left{ src: "/images/home/icons-pattern-left.svg", alt: "Gitlab hero border pattern left svg" }
  %img.image-border.image-border-right{ src: "/images/home/icons-pattern-right.svg", alt: "Gitlab hero border pattern right svg" }
  .header-content
    %img.hero-image-huge{ src: "/images/application-security.svg", alt: "GitLab DevSecOps graphic" }
    %h1 Application Security
    %p DevOps challenges traditional security practices. Learn how to integrate security into the DevOps lifecycle.
    %a.btn.cta-btn.orange.devops-cta{ href: "/resources/whitepaper-seismic-shift-application-security/" } Download the whitepaper

.wrapper.wrapper--large-fonts
  #content.devops-content.u-margin-top-md
    .wrapper.container.js-in-page-nav-group{role: "main"}
      .row.u-margin-top-lg.js-in-page-nav-section#what-is-application-security
        .col-md-10.col-md-offset-1
          %h2.u-text-brand What is application security?

          %p Application security has been <a href="https://searchsoftwarequality.techtarget.com/definition/application-security">defined by TechTarget</a> as the use of software, hardware, and procedural methods to protect applications from external threats. This includes building security measures (called countermeasures) into applications, and also means building security into your development and operational processes: Effectively turning your DevOps methodology into a DevSecOps methodology.

      .row.u-margin-top-lg.js-in-page-nav-section#why-adopt-devsecops
        .col-md-10.col-md-offset-1
          %h2.u-text-brand Why adopt DevSecOps?

          %p A number of cyberattacks dominated international headlines over the last decade and the annual breach count continues to rise, while web application and software vulnerabilities are the top two targets of external attacks. As applications have proliferated into every area of business (and life), a sense of urgency for security is paramount.

          %p Security has traditionally come at the end of the development lifecycle, adding cost and time when code is inevitably sent back to the developer for fixes. It is time to shift left by incorporating security into the DevOps lifecycle - to operate in a DevSecOps model. Security should be a priority at every phase of software development, and every team member should feel empowered to contribute. This intentional and proactive style of security should continue through deployment with the use of tools and methods to protect and monitor live applications.

          %p DevSecOps weaves security practices into every stage of software development. DevSecOps tools automate security workflows to create an adaptable process for your development and security teams, making it easy for both to work in sync and understand what the other is doing. It brings security to the speed of business, consistently securing fast moving and iterative processes - improving efficiency without sacrificing quality, and vice versa.


      .row.u-margin-top-lg.js-in-page-nav-section#benefits-of-devsecops
        .cta-featured-grid
          .row.u-margin-bottom-md
            .col-md-10.col-md-offset-1
              %h2.u-text-brand Benefits of DevSecOps
              .featured-icons.text-center
                .featured-icon
                  = partial "/includes/icons/time-icon2.svg"
                  %p.number Speed
                  %p.description Developers can remediate vulnerabilities while they’re coding, which teaches secure code writing and reduces back and forth during security reviews.
                .featured-icon
                  = partial "/images/icons/first-look-influence.svg"
                  %p.number Collaboration
                  %p.description Encouraging a security mindset across your app dev team aligns goals with security and encourages employees to work with others outside their functional silo.
                .featured-icon
                  = partial "/images/icons/solid-icons/trending-icon2.svg"
                  %p.number Efficiency
                  %p.description DevSecOps saves time, money, and employee resources over every launch and iteration - all valuable assets to IT and security teams suffering from <a href="https://go.forrester.com/blogs/security-risk-2019-cybersecuritys-staffing-shortage-is-self-inflicted/">skills</a> and budget shortages.


      .row.u-margin-top-sm.js-in-page-nav-section#devsecops-fundamentals
        .col-md-10.col-md-offset-1
          %h2.u-text-brand DevSecOps fundamentals

          %p Time saved is important, but DevSecOps is also critical to improving the security posture of all of your software and apps. The first step to mastering DevSecOps fundamentals is integrating security into the earliest possible phases of your DevOps lifecycle. By including security as a standard requirement (rather than a bolt-on task at the end), your business becomes more resilient to external attacks, internal threats, and other unusual or unexpected behaviors within the software.

          %h3.u-margin-top-0 Teamwork

          %p While DevSecOps is in the same family as DevOps, it does require a different mindset from your team. Shifting left can bring up the question: Who <em>actually</em> owns security?

          %p Even with clear ownership, the mindset of responsibility may require a cultural change to get everyone on the same page - thinking about security. Bring the following ideas and actions to your team to ease the transition to DevSecOps:

          %ul
            %li Educate developers on how to write secure code, and how doing so allows for secure functionality through future updates, ease of monitoring app behavior, and can help protect the app or software through potential infrastructure changes.
            %li Help developers understand how they are contributing to an overall business initiative: Their work is on the front line when it comes to protecting both the business and the customer. They have the power to substantially improve the cyberdefense of their business, which both reduces business liability and improves customer trust.
            %li To mitigate potential friction, make clear the division of tactical responsibilities between developers and security team members. Ensure everyone knows what they are responsible for and that they have the tools and resources to do it right.
            %li Encourage collaboration between dev and sec. Future of work predictions call for fluid project teams that bring together employees from across the business. Get ahead of this trend by breaking down some siloes now: Developers should feel welcome to ask for help from their security peers, and security should feel welcome to do the same.

          %h3.u-margin-top-sm Transparency

          %p A single source of truth that reports vulnerabilities and remediation provides much-needed transparency to all parties involved. Keeping development and security within the same tool will further streamline cycles and increase/speed up security adoption among developers. A single source of truth will also prove helpful past production and into deployment, by helping to monitor app and user behavior for suspicious activity.

          %h3.u-margin-top-0 Testing

          %p In DevSecOps, testing can be applied to all phases of the life cycle. <a href="https://about.gitlab.com/developer-survey/">According to our 2019 Developer Survey</a>, most developers test <em>less than half</em> of their code with application security methods. The code that is tested is most commonly reviewed with the following:

          %img.u-margin-bottom-sm{ src: "/images/application-security-survey-graph.png" }

          %p There is always more to be done when it comes to testing, but using every type of test is unrealistic for most teams. It’s best to understand which tests work best for you (this can depend on app or software function, development processes, infrastructure, etc.), and incorporate those into your DevSecOps practice.


          %div.u-image-bg.u-release.u-text-light.u-margin-top-lg{ style: "background-image:url('/images/scaling-devops-bg.jpg');" }
            .container
              .row.u-margin-top-lg.u-margin-bottom-lg
                .col-md-10.col-md-offset-1
                  %h2 Manage your toolchain before it manages you

                  %p.u-margin-top-xs Visible, secure, and effective toolchains are difficult to come by due to the increasing number of tools teams use, and it’s placing strain on everyone involved. This study dives into the challenges, potential solutions, and key recommendations to manage this evolving complexity.

                  .btn-group
                    %a.btn.cta-btn.purple-reverse{ href: "https://about.gitlab.com/resources/whitepaper-forrester-manage-your-toolchain/" } Read the study



      .row.u-margin-top-lg.js-in-page-nav-section#resources
        .col-md-10.col-md-offset-1

          %h2.u-text-brand Resources

          %p Here’s a list of resources on DevSecOps that we find to be particularly helpful in understanding Agile and implementation. We would love to get your recommendations on books, blogs, videos, podcasts and other resources that tell a great Agile story or offer valuable insight on the definition or implementation of the practice.

          %p
            Please share your favorites with us by tweeting us
            %a{:href => 'https://twitter.com/gitlab'} @GitLab!

          .feature-group.feature-group--alt.u-margin-top-lg.u-padding-top-0.u-padding-bottom-0
            .row.flex-row
              .col-md-4.col-lg-4.u-margin-bottom-sm
                .feature.js-all-clickable
                  .feature-media
                    = image_tag "/images/feature-thumbs/feature-thumb-workflow.jpg"
                  .feature-body
                    %h3.feature-title Why you need static and dynamic application security testing in your development workflows
                    %p.feature-description Bolster your code quality with static and dynamic application security testing.
                    %a.feature-more{ href: "/2019/08/12/developer-intro-sast-dast/" } Read

              .col-md-4.col-lg-4.u-margin-bottom-sm
                .feature.js-all-clickable
                  .feature-media
                    = image_tag "/images/feature-thumbs/feature-thumb-4-ways-developers-secure.jpg"
                  .feature-body
                    %h3.feature-title 4 Ways developers can write secure code with GitLab
                    %p.feature-description GitLab Secure is not just for your security team - it’s for developers too.
                    %a.feature-more{ href: "/2019/09/03/developers-write-secure-code-gitlab/" } Read

              .col-md-4.col-lg-4.u-margin-bottom-sm
                .feature.js-all-clickable
                  .feature-media
                    = image_tag "/images/feature-thumbs/feature-thumb-5-security-testing-principles.jpg"
                  .feature-body
                    %h3.feature-title 5 Security testing principles every developer should know
                    %p.feature-description Developers are looking for guidance and standard practices as they take on more security testing responsibilities.
                    %a.feature-more{ href: "/2019/09/16/security-testing-principles-developer/" } Read
